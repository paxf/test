// 'use strict';
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var router = express.Router();
var Book = require('./bookdb.js');

router.get('/', function (req, res) {
	console.log('from books', req.data);
	Book.find()
		.then(books => res.render('books', {books: books}))
		.catch(err => res.render('error', {message: err}));
});

router.post('/', function (req, res) {
	var titleFilter = new Promise(function (resolve, reject) {
		if(!req.body.title || !req.body.desc){
			reject('nhap vao ky tu');
		}
		if(req.body.title.length < 4) {
			reject('nhap title nhieu hon 4 ky tu');
		}
		resolve();
	});

	var findByTitle = () => Book.findOne({title: req.body.title}).exec();
	var checkTitle = data => { if (data) throw 'title da ton tai'; }
	var savingBook = () => new Book({title: req.body.title, desc: req.body.desc}).save();
	var goToBookList = book => res.redirect('/books')
	var renderErrorPage = err => res.render('error', {message: err});

	titleFilter
		.then(findByTitle)
		.then(checkTitle)
		.then(savingBook)
		.then(goToBookList)
		.catch(renderErrorPage);
});

//PUT
router.post('/:book_id', function (req, res) {
	var titleFilter = new Promise((resolve, reject) => {
		if( !req.body.title || !req.body.desc) {
			reject('nhap vao ky tu');
		}
		if(req.body.title.length < 4){
			reject('nhap title nhieu hon 4 ky tu');
		}
		resolve();
	});

	var findByTitle = () => Book.findOne({title: req.body.title}).exec();
	var checkTitle = data => { if (data) throw 'title da ton tai'; }
	var findById = () => Book.findOne({ _id: req.params.book_id }).exec();
	var updateBook = (book) => {
		for(var key in req.body)
			book[key] = req.body[key];
		return book.save();
	};
	var goToBookList = book => res.redirect('/books');
	var renderErrorPage = err => res.render('error', { message: err });

	titleFilter
		.then(findByTitle)
		.then(checkTitle)
		.then(findById)
		.then(updateBook)
		.then(goToBookList)
		.catch(renderErrorPage);
});

//get edit page
router.get('/edit/:book_id', function (req, res) {
	res.render('edit', { id: req.params.book_id });
});

router.get('/delete/:book_id', function (req, res) {
	Book.remove({_id: req.params.book_id}).exec()
		.then(() => { res.redirect('/books') })
		.catch(err => res.render('error', {message: err}));
});

module.exports = router;
