var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.connect('mongodb://localhost:27017/test');
var bookSchema = new Schema({title: {type: String, required: true}, desc: {type: String, required: true}});
var Book = mongoose.model('book', bookSchema);
module.exports = Book;
